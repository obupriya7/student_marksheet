let studentMarks = require("../student-marks")

const Router = (fastify,Option,done) =>{

    // to view all Student Marks Card using GET
    fastify.get('/getStudentMarks',(req,res)=>{
        res.code(200).send(studentMarks);
    })

    // to add a new Student Marks using POST 
    fastify.post('/addStudentMarks',(req,res)=>{
        const {Student_Name,Student_ID,Subject_1,Subject_2,Subject_3,Subject_4,Subject_5}=req.body;
        studentMarks.push({"Student_Name" : Student_Name,"Student_ID" : Student_ID,"Subject_1" : Subject_1,"Subject_2" : Subject_2,"Subject_3" : Subject_3,"Subject_4" : Subject_4,"Subject_5" : Subject_5});
        res.code(201).send(studentMarks);
    })

    // to Update a Student Marks using POST
    fastify.post('/updateStudentMarks/:Student_ID',(req,res)=>{
        const {Student_ID} = req.params
        const {Student_Name,Subject_1,Subject_2,Subject_3,Subject_4,Subject_5} = req.body
        
        studentMarks = studentMarks.map(item =>(item.Student_ID === Student_ID ? {Student_Name,Student_ID,Subject_1,Subject_2,Subject_3,Subject_4,Subject_5} : item))
        item =  studentMarks.find (item => item.Student_ID === Student_ID)
        
        res.code(201).send(item)
    })

    // to delete a Specified Student Marks using DELETE
    fastify.delete('/removeStudentMarks/:Student_ID',(req,res)=>{
        const {Student_ID}=req.params 
        studentMarks = studentMarks.filter (item => item.Student_ID !== Student_ID)
        res.code(200).send({message:'Item '+ Student_ID +' has been removed.'});
        
    })
    done();
}
module.exports = Router;